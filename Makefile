# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/14 06:45:34 by pmatle            #+#    #+#              #
#    Updated: 2017/12/19 17:57:33 by pmatle           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf

SRC = read_map.c array_count.c array_malloc.c free_2d.c get_dimensions.c\
	free_data.c get_map.c invalid_map.c check_basic_errors.c free_map.c\
	cartesian_2_isometric.c start_fdf.c draw_map.c get_points.c\
	move_up.c move_down.c move_left.c move_right.c movements.c get_color.c\
	drawline.c find_index.c free_points.c\

OBJ = read_map.o array_count.o array_malloc.o free_2d.o get_dimensions.o\
	free_data.o get_map.o invalid_map.o check_basic_errors.o free_map.o\
	cartesian_2_isometric.o start_fdf.o draw_map.o get_points.o\
	move_up.o move_down.o move_left.o move_right.o movements.o get_color.o\
	drawline.o find_index.o free_points.o\

CFLAGS = -Wall -Wextra -Werrar

MLX_FLAGS = -lmlx -framework OpenGL -framework AppKit

LIB = -L libft -lft

GCC = gcc

HEADER = fdf.h

all: $(NAME)

$(NAME):
	@make -C libft
	@gcc -c $(SRC)
	gcc $(OBJ) $(LIB) -o $(NAME) $(MLX_FLAGS)

clean:
	@make clean -C libft
	@/bin/rm -f $(OBJ)

fclean: clean
	@make fclean -C libft
	@/bin/rm -f $(NAME)

re: fclean all

norm:
	norminette $(SRC) $(HEADER)
