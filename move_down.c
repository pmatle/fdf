/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_down.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 12:32:41 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 17:34:08 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		increment_y(t_point **points)
{
	t_point		*temp;

	temp = (*points);
	while (temp != NULL)
	{
		temp->y = temp->y + 50;
		temp = temp->next;
	}
	return (1);
}

int		move_down(t_infor *infor)
{
	t_point	*iso;

	iso = infor->points;
	increment_y(&iso);
	draw_map(*infor);
	return (1);
}
