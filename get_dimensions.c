/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_dimensions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 18:29:41 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/14 06:37:06 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		get_dimensions(char *file, int *row, int *col)
{
	char	*line;
	char	**list;
	int		fd;
	int		flag;

	flag = 0;
	fd = open(file, O_RDONLY);
	while (get_next_line(fd, &line))
	{
		if (flag == 0)
		{
			list = ft_strsplit(line, ' ');
			*col = array_count(list);
			free_2d(list);
		}
		flag++;
	}
	*row = flag;
	close(fd);
	return (1);
}
