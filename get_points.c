/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_points.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 11:39:30 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 18:37:44 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			add_points(t_point **head, int row, int col, t_data data)
{
	t_point		*new;
	t_point		*temp;
	int			dir;

	dir = data.map[row][col];
	if (!(new = cartesian_2_isometric(row * 8, col * 8)))
		return (0);
	new->y = (new->y - (dir * 3)) + 200;
	new->x = new->x + 200;
	new->color = get_color(dir);
	new->index.row = row;
	new->index.col = col;
	if (*head == NULL)
		(*head) = new;
	else
	{
		temp = (*head);
		while (temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = new;
	}
	return (1);
}

t_point		*get_points(t_data data)
{
	int			x;
	int			y;
	t_point		*head;

	y = 0;
	head = NULL;
	while (y < data.row)
	{
		x = 0;
		while (x < data.col)
		{
			if (!(add_points(&head, y, x, data)))
				return (0);
			x++;
		}
		y++;
	}	
	return (head);
}