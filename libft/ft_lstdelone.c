/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/25 13:28:43 by pmatle            #+#    #+#             */
/*   Updated: 2017/07/25 14:19:52 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdelone(t_list **alst, void (*del)(void*, size_t))
{
	t_list	*link;

	if (*alst != NULL)
	{
		link = (*alst);
		(*alst) = (*alst)->next;
		(*del)(link->content, link->content_size);
		free(link);
		link = NULL;
	}
}
