/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cartesian_2_isometric.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 09:10:25 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/18 13:44:42 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point		*cartesian_2_isometric(int row, int col)
{
	t_point		*isometric;

	if (!(isometric = (t_point*)ft_memalloc(sizeof(*isometric))))
		return (NULL);
	isometric->x = col - row;
	isometric->y = (col + row) / 2;
	isometric->next = NULL;
	return (isometric);
}
