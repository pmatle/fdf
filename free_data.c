/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_data.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 08:16:21 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/15 16:50:10 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		free_data(t_data data, char **list, char *line)
{
	free_map(data);
	free_2d(list);
	if (line != NULL)
		free(line);
	return (1);
}
