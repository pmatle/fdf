/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 18:18:38 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/20 10:30:29 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <math.h>
# include <stdio.h>
# include <fcntl.h>
# include <mlx.h>
# include "libft/includes/libft.h"

# define ABS(N) ((N < 0) ? (-N) : (N))

typedef struct	s_index
{
	int		row;
	int		col;
}				t_index;

typedef struct	s_point
{
	int				x;
	int				y;
	int				color;
	t_index			index;
	struct s_point	*next;
}				t_point;

typedef struct	s_data
{
	int		row;
	int		col;
	int		**map;
}				t_data;

typedef struct	s_infor
{
	t_data		data;
	t_point		*points;
	void		*mlx;
	void		*win;
}				t_infor;

typedef struct	s_coord
{
	int			x;
	int			y;
}				t_coord;

typedef struct	s_points
{
	int		x;
	int		y;
}				t_points;

int				array_count(char **array);
int				get_dimensions(char *file, int *row, int *col);
int				**array_malloc(int row, int col);
int				**get_map(t_data data, char *file);
t_point			*cartesian_2_isometric(int row, int col);
int				start_fdf(t_infor infor);
t_point			*get_points(t_data data);
int				draw_map(t_infor infor);
int				move_up(t_infor *infor);
int				move_down(t_infor *infor);
int				move_left(t_infor *infor);
int				move_right(t_infor *infor);
int				movements(t_infor *infor, int keycode);
int				get_color(int altitude);
int				drawline(t_infor infor, int x1, int y1, int x2, int y2);
int				find_index(t_infor infor, t_coord *coord, int row, int col);

int				check_basic_errors(char *file);
int				invalid_map(t_data data, char *line, int row);

int				free_2d(char **array);
int				free_map(t_data data);
int				free_data(t_data data, char **list, char *line);
int				free_points(t_point *points);

#endif
