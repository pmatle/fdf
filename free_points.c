/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_points.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/19 17:55:06 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 18:37:52 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		free_points(t_point *points)
{
	t_point		*temp;

	while (points != NULL)
	{
		temp = points;
		points = points->next;
		free(points);
	}
	return (1);
}