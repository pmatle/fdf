/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   movements.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 12:45:31 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 17:30:16 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		movements(t_infor *infor, int keycode)
{
	if (keycode == 126)
	{
		mlx_clear_window(infor->mlx, infor->win);
		move_up(infor);
	}
	if (keycode == 125)
	{
		mlx_clear_window(infor->mlx, infor->win);
		move_down(infor);
	}
	if (keycode == 123)
	{
		mlx_clear_window(infor->mlx, infor->win);
		move_left(infor);
	}
	if (keycode == 124)
	{
		mlx_clear_window(infor->mlx, infor->win);
		move_right(infor);
	}
	return (1);
}