/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   invalid_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 08:26:35 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/15 16:53:31 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		invalid_map(t_data data, char *line, int row)
{
	char	**list;

	list = ft_strsplit(line, ' ');
	if (array_count(list) != data.col)
	{
		free_data(data, list, line);
		ft_putendl_fd("Error: Invalid fdf map", 2);
		exit(0);
	}
	if (row > data.row)
	{
		free_data(data, NULL, NULL);
		ft_putendl_fd("Error: Invalid fdf map", 2);
		exit(0);
	}
	return (1);
}
