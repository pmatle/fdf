/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_index.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/19 09:34:28 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 17:51:49 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		find_index(t_infor infor, t_coord *coord, int row, int col)
{
	t_point		*point;

	point = infor.points;
	if (row < infor.data.col && col < infor.data.col)
	{
		while (point != NULL)
		{
			if (row == point->index.row && col == point->index.col)
			{
				coord->x = point->x;
				coord->y = point->y;
				return (1);
			}
			point = point->next;
		}
	}
	return (0);
}
