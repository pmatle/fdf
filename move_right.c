/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_right.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 12:38:24 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 17:28:23 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		increment_x(t_point **point)
{
	t_point		*temp;

	temp = (*point);
	while (temp != NULL)
	{
		temp->x = temp->x + 50;
		temp = temp->next;
	}
	return (1);
}

int		move_right(t_infor *infor)
{
	int		x;
	int		y;
	t_point	*iso;

	y = 0;
	iso = infor->points;
	increment_x(&iso);
	draw_map(*infor);
	return (1);
}
