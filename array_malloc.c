/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array_malloc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 18:21:03 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/14 18:49:25 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		**array_malloc(int row, int col)
{
	int		**array;
	int		x;

	x = 0;
	if (!(array = (int**)ft_memalloc(sizeof(*array) * row)))
		return (0);
	while (x < row)
	{
		if (!(array[x] = (int*)ft_memalloc(sizeof(**array) * col)))
			return (0);
		x++;
	}
	return (array);
}
