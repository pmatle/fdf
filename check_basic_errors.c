/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_basic_errors.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 07:33:59 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/16 12:40:34 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		check_basic_errors(char *file)
{
	int		fd;
	char	test[10];

	if ((fd = open(file, O_RDONLY)) == -1)
	{
		ft_putendl_fd("Error: Invalid file", 2);
		exit(0);
	}
	if (read(fd, test, 10) == -1)
	{
		ft_putendl_fd("Error: ecountered a problem while reading file", 2);
		exit(0);
	}
	return (1);
}
