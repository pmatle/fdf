/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 18:16:41 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 18:00:50 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		main(int argc, char **argv)
{
	t_infor	infor;

	if (argc == 2)
	{
		check_basic_errors(argv[1]);
		get_dimensions(argv[1], &(infor.data.row), &(infor.data.col));
		infor.data.map = get_map(infor.data, argv[1]);
		if (!(infor.points = get_points(infor.data)))
			return (0);
		if (infor.data.map == NULL || infor.points == NULL)
			return (0);
		start_fdf(infor);
	}
	return (0);
}
