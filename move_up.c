/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_up.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 11:17:36 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 17:38:55 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		decrement_y(t_point **points)
{
	t_point		*temp;

	temp = (*points);
	while (temp != NULL)
	{
		temp->y = temp->y - 50;
		temp = temp->next;
	}
	return (1);
}

int		move_up(t_infor *infor)
{
	int		x;
	int		y;
	t_point	*iso;

	y = 0;
	iso = infor->points;
	decrement_y(&iso);
	draw_map(*infor);
	return (1);
}
