/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start_fdf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 09:21:47 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 17:59:43 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		handle_keys(int keycode, void *data)
{
	t_infor		*infor;

	infor = (t_infor*)data;
	if (keycode == 53)
	{
		if (infor != NULL)
		{
			free_points(infor->points);
			free_map(infor->data);
			mlx_destroy_window(infor->mlx, infor->win);
			exit(0);
		}
	}
	if (keycode >= 123 && keycode <= 126)
		movements(infor, keycode);
	return (1);
}

int		start_fdf(t_infor infor)
{
	infor.mlx = mlx_init();
	infor.win = mlx_new_window(infor.mlx, 750, 480, "FDF");
	draw_map(infor);
	mlx_key_hook(infor.win, handle_keys, &infor);
	mlx_loop(infor.mlx);
	return (1);
}
