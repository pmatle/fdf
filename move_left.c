/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_left.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 12:36:33 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 17:36:22 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		decrement_x(t_point **points)
{
	t_point		*temp;

	temp = (*points);
	while (temp != NULL)
	{
		temp->x = temp->x - 50;
		temp = temp->next;
	}
	return (1);
}

int		move_left(t_infor *infor)
{
	t_point	*iso;

	iso = infor->points;
	decrement_x(&iso);
	draw_map(*infor);
	return (1);
}
