/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 09:55:03 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 17:09:39 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		draw_map(t_infor infor)
{
	int		x;
	int		y;
	t_point	*iso;
	t_coord	first;
	t_coord	second;

	y = 0;
	iso = infor.points;
	while (y < infor.data.row)
	{
		x = 0;
		while (x < infor.data.col)
		{
			first.x = iso->x;
			first.y = iso->y;
			if (find_index(infor, &second, iso->index.row, iso->index.col + 1))
				drawline(infor, first.x, first.y, second.x, second.y);
			if (find_index(infor, &second, iso->index.row + 1, iso->index.col))
				drawline(infor, first.x, first.y, second.x, second.y);
			x++;
			iso = iso->next;
		}
		y++;
	}
	return (1);
}
