/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 07:28:32 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 18:22:49 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		**process_line(int **map, char *line, int row)
{
	char	**list;
	char	**list1;
	int		x;
	int		some;

	x = 0;
	list = ft_strsplit(line, ' ');
	while (list[x])
	{
		map[row][x] = ft_atoi(list[x]);
		x++;
	}
	free_2d(list);
	return (map);
}

int		**get_map(t_data data, char *file)
{
	char	**list;
	char	*line;
	int		fd;
	int		row;

	row = 0;
	if ((fd = open(file, O_RDONLY)) == -1)
	{
		ft_putendl_fd("Error: Could not open file", 2);
		exit(0);
	}
	data.map = array_malloc(data.row, data.col);
	while (get_next_line(fd, &line) == 1)
	{
		invalid_map(data, line, row);
		data.map = process_line(data.map, line, row);
		row++;
		free(line);
	}
	if (row != data.row)
	{
		free_data(data, NULL, NULL);
		exit(0);
	}
	return (data.map);
}
