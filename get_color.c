/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/16 14:03:08 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/19 18:50:59 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		get_color(int altitude)
{
	int		color;

	if (altitude > -100 && altitude <= 20)
		color = 0x00B89E8C;
	else if (altitude > 20 && altitude <= 50)
		color = 0x00B8713D;
	else if (altitude > 50 && altitude <= 100)
		color = 0x00E56A10;
	else if (altitude > 100 && altitude <= 150)
		color = 0x00C19C30;
	else if (altitude > 150 && altitude <= 200)
		color = 0x00C18730;
	else if (altitude > 200 && altitude <= 250)
		color = 0x00C17A30;
	else if (altitude > 250 && altitude <= 300)
		color = 0x00C16530;
	else if (altitude > 300 && altitude <= 350)
		color = 0x00C14D30;
	else if (altitude > 350)
		color = 0x00C60505;
	return (color);
}
